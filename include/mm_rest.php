<?php
class mm_rest
{
  public function __construct()
  {
    $this->mm = new RestClient([
        'base_url' => "https://marketplace.madeiramadeira.com.br/v1/",
        // 'format' => "json",
        'headers' => [
          'content-type' => 'application/json',
          'TOKENMM' => MMTOKEN
        ]
        ]);
  }

  public function decode_return($object) {
    return json_decode($object->response);
  }
}
 ?>
