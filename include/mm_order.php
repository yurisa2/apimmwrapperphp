<?php
class MMOrder extends mm_rest
{
  public function mmGetOrder($orderId)
  {
    $return = $this->mm->get("pedido/id/$orderId");

    return $this->decode_return($return);
  }

  public function mmGetOrders()
  {
    $return = $this->mm->get("pedido/received");

    return $this->decode_return($return);
  }

  public function mmGetOrderList()
  {
    $orders = $this->mmGetOrders();

    if(is_null($orders) || !$orders) return false;

    foreach ($orders->data as $key => $value) {
      $arrayId[] = $value->id;
    }

    return $arrayId;
  }

  public function mmNotifyMktOrder($orderId)
  {
    return $this->mm->put("pedido/$orderId/received");
  }

}
?>
