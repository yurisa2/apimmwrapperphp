<?php
class MMProduct extends mm_rest
{
  public function mmGetProducts()
  {
    $return = $this->mm->get("produto/situacao/publicados/limit=100&offset=0");
    // $return = $this->mm->get("produto/situacao/pendentes/limit=100&offset=0");

    return $this->decode_return($return);
  }

  public function mmGetProductList()
  {
    $productList = $this->mmGetProducts();

    if(is_null($productList) || !$productList) return false;

    foreach ($productList->data as $key => $value) {
      $arraysku[] = $value->sku;
    }

    return $arraysku;
  }

  public function mmGetProduct($productSku)
  {
    // $return = $this->mm->get("produto/situacao/publicados/limit=100&offset=0");
    $return = $this->mm->get("produto/$productSku");

    return $this->decode_return($return);
  }

  public function mmGetProductPrice($productSku)
  {

    return $this->decode_return($this->mm->get("produto/preco/$productSku"));
  }

  public function mmUpdateProductPrice($productSku,$productPrice)
  {
    $productEntity = '[{
        "sku": "'.$productSku.'",
        "preco_de": '.(float)$productPrice.',
        "preco_por": '.(float)$productPrice.'
      }]';

    return $this->decode_return($this->mm->put("produto/preco",$productEntity));
  }

  public function mmUpdateProductStock($productSku,$productStock)
  {
    $productEntity = '[{
        "sku": "'.$productSku.'",
        "estoque": '.(int)$productStock.'
      }]';

    return $this->decode_return($this->mm->put("produto/estoque",$productEntity));
  }

  public function mmUpdateProductBulk($productSku,$productPrice,$productStock)
  {
    $productEntity = '[{
    "sku": "'.$productSku.'",
    "preco_de": '.(float)$productPrice.',
    "preco_por": '.(float)$productPrice.',
    "estoque": '.$productStock.'
  }]';


    return $this->decode_return($this->mm->put("produto/bulk",$productEntity));
  }

}
?>
