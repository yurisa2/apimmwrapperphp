<?php
if(!isset($prefix_mm)) $prefix_mm = '';

if(file_exists($prefix_mm.'include/php-restclient/vendor/autoload.php')) require_once $prefix_mm.'include/php-restclient/vendor/autoload.php';

require_once $prefix_mm.'include/config.php';
require_once $prefix_mm.'include/defines.php';

require_once $prefix_mm.'include/mm_rest.php';
require_once $prefix_mm.'include/mm_product.php';
require_once $prefix_mm.'include/mm_order.php';

 ?>
